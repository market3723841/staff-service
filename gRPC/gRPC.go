package grpc

import (
	"gitlab.com/market3723841/staff-service/config"
	"gitlab.com/market3723841/staff-service/gRPC/client"
	"gitlab.com/market3723841/staff-service/gRPC/service"
	staff_service "gitlab.com/market3723841/staff-service/genproto"
	"gitlab.com/market3723841/staff-service/pkg/logger"
	"gitlab.com/market3723841/staff-service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	staff_service.RegisterStaffTariffServiceServer(grpcServer, service.NewStaffTariffService(cfg, log, strg, srvc))
	staff_service.RegisterStaffServiceServer(grpcServer, service.NewStaffService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
